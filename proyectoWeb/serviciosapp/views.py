from django.shortcuts import render
from serviciosapp.models import Servicio


# Create your views here.

def servicios(request):
    
    servicios = Servicio.objects.all() #IMPORTAME TODOS LOS OBJETOS QUE TENGO CREADOS EN LA CLASE SERVICIOS

    return render(request,"servicios/servicios.html", {"servicios":servicios})
